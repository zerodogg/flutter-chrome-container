# flutter CI image with chrome

This has **moved** to https://gitlab.com/mglog/flutter-chrome-container

Since images won't be updated here, they have been deleted to avoid having
outdated images used. Update any `.gitlab-ci.yml` to use the new URL.
